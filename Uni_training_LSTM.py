import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf
from tensorflow import keras
from keras.preprocessing.sequence import TimeseriesGenerator

from keras.models import Sequential
from keras.layers import LSTM, Dense
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse
from keras.models import load_model
import warnings
warnings.filterwarnings("ignore")


#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

#fichier='Input BR.xlsx'
file='Input.xlsx'
sheetname = 'pred'

#file = path+fichier

df = pd.read_excel(file, sheetname)


df['Date'] = pd.to_datetime(df["Date"])
df.set_axis(df['Date'], inplace=True)
df1=df
df=df.iloc[:,0:2]

# Split dataset
close_data = df['IP invoiced traffic Gbps'].values
close_data = close_data.reshape((-1,1))

split_percent = 0.80
split = int(split_percent*len(close_data))

close_train = close_data[:split]
close_test = close_data[split:]

date_train = df['Date'][:split]
date_test = df['Date'][split:]

look_back =5

train_generator = TimeseriesGenerator(close_train, close_train, length=look_back, batch_size=1)     
test_generator = TimeseriesGenerator(close_test, close_test, length=look_back, batch_size=5)

model = Sequential()
model.add(
    LSTM(20,
        activation='relu',
        input_shape=(look_back,1))
)
model.add(Dense(1))
model.compile(optimizer='adam', loss='mse')

num_epochs = 80
model.fit(train_generator, epochs=num_epochs, verbose=1)

prediction = model.predict(test_generator)


date_test1=pd.DataFrame(date_test).rename(columns={0:"Date"})
date_test1.reset_index(drop=True, inplace=True)
prediction1=pd.DataFrame(prediction).rename(columns={0:"lstm_predictions"})
lstm_pred=pd.concat((date_test1,prediction1), axis=1)
lstm_pred=pd.concat((date_test1,prediction1), axis=1)
lstm_pred.reset_index(drop=True, inplace=True)
lstm_pred['Date'] = pd.to_datetime(lstm_pred['Date'])
lstm_pred.set_axis(lstm_pred['Date'], inplace=True)
test=pd.concat((df['IP invoiced traffic Gbps'],lstm_pred), axis=1).iloc[len(close_train):len(df)-look_back,:]
print(test)
lstm_rmse_error = rmse(test['IP invoiced traffic Gbps'], test["lstm_predictions"])
lstm_mse_error = lstm_rmse_error**2
mean_value = test['IP invoiced traffic Gbps'].mean()
print("test dataset")
print(f'MSE Error: {lstm_mse_error}\nRMSE Error: {lstm_rmse_error}\nMean: {mean_value}')

# Plot the forecast values 
ax=df['IP invoiced traffic Gbps'].plot(figsize = (12, 5), legend = True, title = "OTI Traffic") 
lstm_pred['lstm_predictions'].plot(legend=True,label='Forecast',)
ax.set(xlabel='Dates', ylabel='Total Trafic')
plt.rcParams['savefig.facecolor']='white'
plt.savefig("iptraffic_uni_lstm.png")

# save the model to disk
#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'
filename =  'LSTM_uni_model.h5'   
model.save(filename)
