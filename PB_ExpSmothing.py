# 'dataset' contient les données d'entrée pour ce script
import pandas as pd
import numpy as np
from datetime import datetime
import joblib
from statsmodels.tsa.holtwinters import ExponentialSmoothing
# load the model to disk
path = ''
filename = path + 'Smoothing_model.sav'
loaded_model = joblib.load(filename)



file='Input.xlsx'

sheetname = 'pred'


dataset = pd.read_excel(file, sheetname)

dataset.set_index(dataset['Date'], inplace=True) 
#dataset.set_index('Date')
#dataset.index.freq='MS'

range = pd.date_range('2022-01-01',periods=24,freq='MS')
predictions = loaded_model.forecast(24)
predictions_range = pd.DataFrame({'OTI':predictions,'Date':range})

dataset=dataset.append(predictions_range)

df3=dataset
df3.Date = pd.to_datetime(df3.Date)
df3 = df3.groupby('Date').OTI.sum()
df3 = pd.DataFrame(df3.groupby(df3.index.to_period('y')).cumsum().reset_index().to_records())
df3.columns=['index','Date','Cumul_OTI']
df3=df3.drop('index', axis=1)

dataset=pd.merge(dataset,df3, on=["Date", "Date"])
print(dataset.head(10))
