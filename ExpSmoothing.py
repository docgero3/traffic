import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse
import joblib
from sklearn import model_selection
import warnings
warnings.filterwarnings("ignore")
#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

file='Input.xlsx'

sheetname = 'pred'

#file = path+fichier
# DataFrame df:
df = pd.read_excel(file, sheetname)

#close_data = df.reset_index()['IP invoiced traffic Gbps']
df['Date'] = pd.to_datetime(df["Date"])
df.set_axis(df['Date'], inplace=True)
df1=df


##splitting dataset into train and test split
split_percent = 0.80
split = int(len(df)*split_percent)
train_data,test_data = df[:split],df[split:]

date_train = df['Date'][:split]
date_test = df['Date'][split:]
model = ExponentialSmoothing(train_data['IP invoiced traffic Gbps'],trend='mul',seasonal='mul',seasonal_periods=12).fit()
smoothing_pred=model.forecast(len(date_test)).rename("Predictions")

smoothing_rmse_error = rmse(test_data['IP invoiced traffic Gbps'], smoothing_pred)
smoothing_mse_error = smoothing_rmse_error**2
mean_value = df['IP invoiced traffic Gbps'].mean()
print("test dataset")
print(f'MSE Error: {smoothing_mse_error}\nRMSE Error: {smoothing_rmse_error}\nMean: {mean_value}')

# save the model to disk
filename = 'Smoothing_model.sav'
joblib.dump(model, filename)
loaded_model = joblib.load(filename)


range = pd.date_range('2023-01-01',periods=24,freq='MS')
predictions = loaded_model.forecast(24)
predictions_range = pd.DataFrame({'IP invoiced traffic Gbps':predictions,'Date':range})
predictions_range.set_axis(predictions_range['Date'], inplace=True)
predictions_range.index.names = ['index']
df1=df.append(predictions_range)

df3=df1
df3.Date = pd.to_datetime(df3.Date)

df3 = df3.groupby('Date')["IP invoiced traffic Gbps"].sum()
df3 = pd.DataFrame(df3.groupby(df3.index.to_period('y')).cumsum().reset_index().to_records())
df3.columns=['index','Date','Cumul_IP']
df3=df3.drop('index', axis=1)

df=pd.merge(df1,df3, on=["Date", "Date"])
print(df.head(10))
