import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
#model
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM

from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse
from keras.models import load_model
#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

#fichier='Input.xlsx'
file='Input.xlsx'

sheetname = 'pred'

#file = path+fichier

df = pd.read_excel(file, sheetname)


close_data = df.reset_index()['IP invoiced traffic Gbps']
df['Date'] = pd.to_datetime(df["Date"])
df.set_axis(df['Date'], inplace=True)
df1=df
### LSTM are sensitive to the scale of the data. so we apply MinMax scaler

scaler = MinMaxScaler(feature_range=(0,1))
close_data = scaler.fit_transform(np.array(close_data).reshape(-1,1))

##splitting dataset into train and test split
split_percent = 0.80
split = int(len(close_data)*split_percent)
train_data,test_data = close_data[:split],close_data[split:]



date_train = df['Date'][:split]
date_test = df['Date'][split:]

#Data Preprocessing
# convert an array of values into a dataset matrix

def create_ds(dataset,time_steps):   #Time Steps is how many previous records considered to predict the current record
    data_x,data_y = [],[]
    for i in range(len(dataset)-time_steps-1):
        a = dataset[i:(i+time_steps),0]
        data_x.append(a)
        b = dataset[i+time_steps,0]
        data_y.append(b)
    return np.array(data_x),np.array(data_y)

time_step = 5
X_train, y_train = create_ds(train_data, time_step)
X_test, ytest = create_ds(test_data, time_step)

#Before feeding into lstm we must convert dataset into 3d 
# reshape input to be [samples, time steps, features] which is required for LSTM
X_train =X_train.reshape(X_train.shape[0],X_train.shape[1] , 1)
X_test = X_test.reshape(X_test.shape[0],X_test.shape[1] , 1)


model=Sequential()
model.add(LSTM(50,return_sequences=True,input_shape=(5,1)))
model.add(LSTM(50,return_sequences=True))
model.add(LSTM(50))
model.add(Dense(1))

model.compile(loss='mean_squared_error',optimizer='adam')
model.fit(X_train,y_train,validation_data=(X_test,ytest),epochs=40,batch_size=1,verbose=1)



train_predict=model.predict(X_train)
test_predict=model.predict(X_test)

# We have scaled it, so we need to reverse scale it to find the o/p
train_predict=scaler.inverse_transform(train_predict)
test_predict=scaler.inverse_transform(test_predict)

#predicting future 24 months
#For predicting next day after last test data, we need to take previous 100 values(timestep)
x_input=test_data[len(ytest)+1:].reshape(1,-1)
x_input.shape

temp_input=list(x_input)
temp_input=temp_input[0].tolist()


look_back=5
date_test1=pd.DataFrame(date_test).rename(columns={0:"Date"})
date_test1.reset_index(drop=True, inplace=True)
prediction1=pd.DataFrame(test_predict).rename(columns={0:"lstm_predictions"})
lstm_pred=pd.concat((date_test1,prediction1), axis=1)
lstm_pred=pd.concat((date_test1,prediction1), axis=1)
lstm_pred.reset_index(drop=True, inplace=True)
lstm_pred['Date'] = pd.to_datetime(lstm_pred['Date'])
lstm_pred.set_axis(lstm_pred['Date'], inplace=True)
test=pd.concat((df['IP invoiced traffic Gbps'],lstm_pred), axis=1).iloc[len(date_train):len(df)-look_back-1,:]
print(test)

#RMSE
lstm_rmse_error = rmse(test['IP invoiced traffic Gbps'], test["lstm_predictions"])
lstm_mse_error = lstm_rmse_error**2
mean_value = test['IP invoiced traffic Gbps'].mean()
print("test dataset")
print(f'MSE Error: {lstm_mse_error}\nRMSE Error: {lstm_rmse_error}\nMean: {mean_value}')


#GRAPH test
ax=df['IP invoiced traffic Gbps'].plot(figsize = (12, 5), legend = True, title = "OTI Traffic") 
lstm_pred['lstm_predictions'].plot(legend=True,label='Forecast',)
ax.set(xlabel='Dates', ylabel='Total Trafic')
plt.rcParams['savefig.facecolor']='white'
plt.savefig("iptraffic_multi_lstm.png")


# save the model to disk
#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'
filename =  'LSTM_multi_model.h5'   
model.save(filename)

