import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf
from tensorflow import keras
from sklearn.preprocessing import MinMaxScaler

#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

file='Input.xlsx'

sheetname = 'pred'

#file = path+fichier

df = pd.read_excel(file, sheetname)
filename = 'LSTM_multi_model.h5' 

LSTMmodel = tf.keras.models.load_model(filename)
#df['Date'] = pd.to_datetime(df["Date"])
#df.set_axis(df['Date'], inplace=True)
#df1=df
df1 = df.reset_index()['IP invoiced traffic Gbps']


### LSTM are sensitive to the scale of the data. so we apply MinMax scaler

scaler = MinMaxScaler(feature_range=(0,1))
df1 = scaler.fit_transform(np.array(df1).reshape(-1,1))

##splitting dataset into train and test split
train_size = int(len(df1)*0.65)
train_data,test_data = df1[0:train_size,:],df1[train_size:len(df1),:1]

#Data Preprocessing
# convert an array of values into a dataset matrix

def create_ds(dataset,time_steps):   #Time Steps is how many previous records considered to predict the current record
    data_x,data_y = [],[]
    for i in range(len(dataset)-time_steps-1):
        a = dataset[i:(i+time_steps),0]
        data_x.append(a)
        b = dataset[i+time_steps,0]
        data_y.append(b)
    return np.array(data_x),np.array(data_y)

time_step = 10
X_train, y_train = create_ds(train_data, time_step)
X_test, ytest = create_ds(test_data, time_step)

#Before feeding into lstm we must convert dataset into 3d 
# reshape input to be [samples, time steps, features] which is required for LSTM
X_train =X_train.reshape(X_train.shape[0],X_train.shape[1] , 1)
X_test = X_test.reshape(X_test.shape[0],X_test.shape[1] , 1)


train_predict=LSTMmodel.predict(X_train)
test_predict=LSTMmodel.predict(X_test)

# We have scaled it, so we need to reverse scale it to find the o/p
train_predict=scaler.inverse_transform(train_predict)
test_predict=scaler.inverse_transform(test_predict)

#predicting future 24 months
#For predicting next day after last test data, we need to take previous 100 values(timestep)
x_input=test_data[len(ytest)+1:].reshape(1,-1)
x_input.shape

temp_input=list(x_input)
temp_input=temp_input[0].tolist()


from numpy import array

lst_output=[]
n_steps=10
i=0
while(i<24):
    
    if(len(temp_input)>10):
        #print(temp_input)
        x_input=np.array(temp_input[1:])
        print("{} month input {}".format(i,x_input))
        x_input=x_input.reshape(1,-1)
        x_input = x_input.reshape((1, n_steps, 1))
        #print(x_input)
        yhat = LSTMmodel.predict(x_input, verbose=0)
        print("{} month output {}".format(i,yhat))
        temp_input.extend(yhat[0].tolist())
        temp_input=temp_input[1:]
        #print(temp_input)
        lst_output.extend(yhat.tolist())
        i=i+1
    else:
        x_input = x_input.reshape((1, n_steps,1))
        yhat = LSTMmodel.predict(x_input, verbose=0)
        print(yhat[0])
        temp_input.extend(yhat[0].tolist())
        print(len(temp_input))
        lst_output.extend(yhat.tolist())
        i=i+1
        
        
day_new=np.arange(1,11)
day_pred=np.arange(11,35)

forecast=pd.DataFrame(scaler.inverse_transform(lst_output))
forecast=forecast.rename(columns={0:"lstm_predictions"})



def predict_dates(num_prediction):
    last_date = df['Date'].values[-1]
    prediction_dates = pd.date_range(last_date, periods=num_prediction+1,freq='MS').tolist()
    return prediction_dates

num_prediction = 24

forecast_dates = predict_dates(num_prediction)
forecast_dates=pd.DataFrame(forecast_dates).iloc[1:,:]
forecast_dates1=forecast_dates.reset_index()
forecast_dates1=forecast_dates1.iloc[:,1:2]
forecast_dates1=forecast_dates1.rename(columns={0:"Date"})


new_pred=pd.concat((forecast_dates1,forecast), axis=1)
new_pred.reset_index(drop=True, inplace=True)
new_pred['Date'] = pd.to_datetime(new_pred['Date'])
new_pred.set_axis(new_pred['Date'], inplace=True)


df4=df
df4.reset_index(drop=True, inplace=True)
df4['Date'] = pd.to_datetime(df4['Date'])
df4.set_axis(df4['Date'], inplace=True)

df4=df4.append(new_pred.rename(columns={"lstm_predictions":"IP invoiced traffic Gbps"}))
df4 = df4.drop(['Date'], axis=1)
df4.info()
