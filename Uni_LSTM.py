import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf
from tensorflow import keras
from keras.preprocessing.sequence import TimeseriesGenerator
#import warnings
#warnings.filterwarnings("ignore")
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

file='Input.xlsx'

sheetname = 'pred'


df = pd.read_excel(file, sheetname)
filename = 'LSTM_uni_model.h5' 

LSTMmodel = tf.keras.models.load_model(filename)

df['Date'] = pd.to_datetime(df["Date"])
df.set_axis(df['Date'], inplace=True)
df1=df
df=df.iloc[:,0:2]

# Split dataset
close_data = df['IP invoiced traffic Gbps'].values
close_data = close_data.reshape((-1,1))

split_percent = 0.80
split = int(split_percent*len(close_data))

close_train = close_data[:split]
close_test = close_data[split:]

date_train = df['Date'][:split]
date_test = df['Date'][split:]

#print(len(close_train))
#print(len(close_test))

look_back =5

train_generator = TimeseriesGenerator(close_train, close_train, length=look_back, batch_size=1)     
test_generator = TimeseriesGenerator(close_test, close_test, length=look_back, batch_size=5)
prediction = LSTMmodel.predict(test_generator)

close_train = close_train.reshape((-1))
close_test = close_test.reshape((-1))
prediction = prediction.reshape((-1))

close_data = df['IP invoiced traffic Gbps'].values
close_data = close_data.reshape((-1,1))

close_data = close_data.reshape((-1))
look_back = 5

def predict(num_prediction, model):
    prediction_list = close_data[-look_back:]
    
    for _ in range(num_prediction):
        x = prediction_list[-look_back:]
        x = x.reshape((1, look_back, 1))
        out = model.predict(x)[0][0]
        prediction_list = np.append(prediction_list, out)
    prediction_list = prediction_list[look_back-1:]
        
    return prediction_list
    
def predict_dates(num_prediction):
    last_date = df['Date'].values[-1]
    prediction_dates = pd.date_range(last_date, periods=num_prediction+1,freq='MS').tolist()
    return prediction_dates

num_prediction = 24
forecast = predict(num_prediction, LSTMmodel)
forecast_dates = predict_dates(num_prediction)

#Concatenation

forecast_dates=pd.DataFrame(forecast_dates).iloc[1:,:]
forecast_dates1=forecast_dates.reset_index()
forecast_dates1=forecast_dates1.iloc[:,1:2]
forecast_dates1=forecast_dates1.rename(columns={0:"Date"})


forecast=pd.DataFrame(forecast)
forecast=forecast.rename(columns={0:"lstm_predictions"})
new_pred=pd.concat((forecast_dates1,forecast), axis=1)
new_pred.reset_index(drop=True, inplace=True)
new_pred['Date'] = pd.to_datetime(new_pred['Date'])
new_pred.set_axis(new_pred['Date'], inplace=True)


df4=df1
df4.reset_index(drop=True, inplace=True)
df4['Date'] = pd.to_datetime(df4['Date'])
df4.set_axis(df4['Date'], inplace=True)

df4=df4.append(new_pred.rename(columns={"lstm_predictions":"IP invoiced traffic Gbps"}))
df4 = df4.drop(['Date'], axis=1)
df4.info()
