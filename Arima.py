import numpy as np
import pandas as pd

from statsmodels.tsa.statespace.sarimax import SARIMAX
import joblib
                     
from sklearn.metrics import mean_squared_error
from statsmodels.tools.eval_measures import rmse
import warnings
import joblib
warnings.filterwarnings("ignore")




#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'

file='Input.xlsx'
#sheetname = 'forecast'

#path = 'C:/Users/lnzb7292/Desktop/BPM/Power BI IC Reports/'
#fichier = 'Input_BR.xlsx'
sheetname = 'pred'

#file = path+fichier

df = pd.read_excel(file, sheetname)
df1=df
df.Date = pd.to_datetime(df.Date)
df = df.set_index("Date")

df.index.freq = 'MS'

split_percent = 0.80
split = int(len(df)*split_percent)
train_data,test_data = df[:split],df[split:]
date_train = df1['Date'][:split]
date_test = df1['Date'][split:]

arima_model = SARIMAX(train_data["IP invoiced traffic Gbps"], order = (2,1,1), seasonal_order = (4,0,3,12))
arima_result = arima_model.fit()
arima_result.summary()

arima_pred = arima_result.predict(start = len(train_data), end = len(df)-1, typ="levels").rename("ARIMA Predictions")

arima_rmse_error = rmse(test_data['IP invoiced traffic Gbps'], arima_pred)
arima_mse_error = arima_rmse_error**2
mean_value = df['IP invoiced traffic Gbps'].mean()

print(f'MSE Error: {arima_mse_error}\nRMSE Error: {arima_rmse_error}\nMean: {mean_value}')

# save the model to disk
filename = 'arima_model.sav'
joblib.dump(arima_model, filename)
result = arima_model.fit()
loaded_model = joblib.load(filename)

# Forecast for the next 1 years 
forecast1 = result.predict(start = len(df), end = (len(df)-1) + 1 * 24, typ = 'levels').rename('IP invoiced traffic Gbps') 


predictions_range=pd.DataFrame(forecast1)
df=df.append(predictions_range)
predictions_range.reset_index(inplace=True)
predictions_range= predictions_range.rename(columns = {'index':'Date'})
df['Date'] = df.index
df=df.reset_index()
df=df.drop('index',axis=1)
df = df.set_index("Date")
print(df.head(10))
