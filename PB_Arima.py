# 'dataset' contient les données d'entrée pour ce script
import pandas as pd
import numpy as np
from datetime import datetime
import joblib
from sklearn import model_selection

# load the model to disk
path = ''
filename = path + 'arima_model.sav'
loaded_model = joblib.load(filename)

file='Input.xlsx'
sheetname = 'pred'
dataset = pd.read_excel(file, sheetname)


dataset.set_index(dataset['Date'], inplace=True) 
#dataset.set_index('Date')
#dataset.index.freq='MS'

result = loaded_model.fit()

# Forecast for the next 2 years 
forecast1 = result.predict(start = len(dataset), end = (len(dataset)-1) + 1 * 24, typ = 'levels').rename('OTI') 
predictions_range=pd.DataFrame(forecast1)
dataset=dataset.iloc[:,1:5]
dataset=dataset.append(predictions_range)
dataset['Date'] = dataset.index
dataset=dataset.reset_index()
dataset=dataset.drop('index',axis=1)


df3=dataset
df3.Date = pd.to_datetime(df3.Date)
df3 = df3.groupby('Date').OTI.sum()
df3 = pd.DataFrame(df3.groupby(df3.index.to_period('y')).cumsum().reset_index().to_records())
df3.columns=['index','Date','Cumul_OTI']
df3=df3.drop('index', axis=1)

dataset=pd.merge(dataset,df3, on=["Date", "Date"])
print(dataset.head(10))
